#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include "pessoa.hpp"
#include <string>

class Professor : public Pessoa{
	//atributos
	string formacao;
	int sala;
	double salario;

public:	
	//metodos
	Professor();
	~Professor();
	
	string getFormacao(); int getSala(); double getSalario();
	void setFormacao(string formacao), setSala(int sala), setSalario(double salario), imprimeDadosProfessor();
};

#endif
